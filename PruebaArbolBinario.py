'''
Created on 27/10/2021

@author: Herrera
'''
from sys import maxunicode

class NodoArbol:
    def __init__(self, dato):
        self.dato = dato
        self.nodoIzquierdo = None
        self.nodoDerecho = None
    
    def __str__(self):
        return str(self.value);
    
class ArbolBinarioBusqueda:
    
    def __init__(self):
        self.raiz = None
    
    def agregarNodo(self, dato):
        nodoNuevo = NodoArbol(dato)
        if(self.raiz == None):
            self.raiz = nodoNuevo
            print("\nSe agrego el dato")
        else:
            nodoPadre = None
            nodoActual = self.raiz
            
            while(nodoActual != None):
                nodoPadre = nodoActual
                if(dato <= nodoActual.dato):
                    nodoActual = nodoActual.nodoIzquierdo
                    if(nodoActual == None):
                        nodoPadre.nodoIzquierdo = nodoNuevo
                        print("\nSe agrego el dato")
                else:
                    nodoActual = nodoActual.nodoDerecho
                    if(nodoActual == None):
                        nodoPadre.nodoDerecho = nodoNuevo
                        print("\nSe agrego el dato")
    
    def recorridoPreOrden(self, raiz):
        if(not(raiz == None)):
            print(f"{raiz.dato} => ", end="")
            ArbolBinarioBusqueda.recorridoPreOrden(self, raiz.nodoIzquierdo)
            ArbolBinarioBusqueda.recorridoPreOrden(self, raiz.nodoDerecho)
            
    def recorridoOrden(self, raiz):
        if(not(raiz == None)):
            ArbolBinarioBusqueda.recorridoOrden(self, raiz.nodoIzquierdo)
            print(f"{raiz.dato} => ", end="")
            ArbolBinarioBusqueda.recorridoOrden(self, raiz.nodoDerecho)
    
    def recorridoPostOrden(self, raiz):
        if(not(raiz == None)):
            ArbolBinarioBusqueda.recorridoPostOrden(self, raiz.nodoIzquierdo)
            ArbolBinarioBusqueda.recorridoPostOrden(self, raiz.nodoDerecho)
            print(f"{raiz.dato} => ", end="")
    
    def eliminarElemento(self, dato):
        
        if(not(self.raiz == None)):
            anterior = self.raiz
            aux = self.raiz
            ladoArbol = ""
            
            while(aux.dato != dato):
                anterior = aux
                if(dato <= aux.dato):
                    aux = aux.nodoIzquierdo
                    ladoArbol = "IZQ"
                else:
                    aux = aux.nodoDerecho
                    ladoArbol = "DER"
                
                if(aux == None):
                    print("\nBuscado y no encontrado")
            
            print(f"\n{dato} encontrado")
            
            if(aux.nodoIzquierdo == None and aux.nodoDerecho == None):
                if(aux == self.raiz):
                    self.raiz = None
                elif(ladoArbol == "IZQ"):
                    anterior.nodoIzquierdo = None
                else:
                    anterior.nodoDerecho = None
                    
            elif (aux.nodoIzquierdo == None):
                if(aux == self.raiz):
                    self.raiz = aux.nodoDerecho
                elif (ladoArbol == "IZQ"):
                    anterior.nodoIzquierdo = aux.nodoDerecho
                else:
                    anterior.nodoDerecho = aux.nodoIzquierdo
            
            elif(aux.nodoDerecho == None):
                if(aux == self.raiz):
                    self.raiz = aux.nodoIzquierdo
                elif(ladoArbol == "IZQ"):
                    anterior.nodoIzquierdo = aux.nodoIzquierdo
                else:
                    anterior.nodoDerecho = aux.nodoIzquierdo
            
            else:
                reemplazo = ArbolBinarioBusqueda.reemplazar(aux)
                
                if(aux == self.raiz):
                    self.raiz = reemplazo
                elif(ladoArbol == "IZQ"):
                    anterior.nodoDerecho = reemplazo
                else:
                    anterior.nodoDerecho = reemplazo
                reemplazo.nodoIzquierdo = aux.nodoIzquierdo
                
            print("\nEliminando . . . ")
            return True
        else:
            print("\nArbol vacio")
            return False
        
    def reemplazar(self, nodo):
        reemplazarPadre = nodo
        reemplazo = nodo
        auxiliar = nodo.nodoDerecho
        
        while(auxiliar != None):
            reemplazarPadre = reemplazo
            reemplazo = auxiliar
            auxiliar = auxiliar.nodoDerecho
            
        if(reemplazo != nodo.nodoDerecho):
            reemplazarPadre.nodoIzquierdo = reemplazo.nodoDerecho
            reemplazarPadre.nodoDerecho = nodo.nodoDerecho
        return reemplazo

    def mostrarDatoMayor(self):
        if(self.raiz == None):
            print("\nArbol vacio")
        else:
            nodoActual = self.raiz
            nodoPadre = None
            while(nodoActual != None):
                nodoPadre = nodoActual
                nodoActual = nodoActual.nodoDerecho
            print(f"\nDato mayor: {nodoPadre.dato}")
            return nodoPadre.dato
    
    def mostrarDatoMenor(self):
        if(self.raiz == None):
            print("\nArbol vacio")
            return 0
        else:
            nodoActual = self.raiz
            nodoPadre = None
            while(nodoActual != None):
                nodoPadre = nodoActual
                nodoActual = nodoActual.nodoIzquierdo
            print(f"\nDato menor: {nodoPadre.dato}")
            return nodoPadre.dato    
    
    def buscarNodo(self, dato):
        if(not(self.raiz == None)):
            auxiliar = self.raiz
            while(auxiliar.dato != dato):
                if(dato < auxiliar.dato):
                    auxiliar = auxiliar.nodoIzquierdo
                else:
                    auxiliar = auxiliar.nodoDerecho
            
                if(auxiliar == None):
                    return False
        
        
                return True
        else:
            return False
                

abb = ArbolBinarioBusqueda()  
'''
abb.agregarNodo(10)
abb.agregarNodo(18)
abb.agregarNodo(8)
abb.agregarNodo(3)
abb.agregarNodo(9)
abb.agregarNodo(1)
abb.agregarNodo(24)
'''

 
opcion = 0
while(opcion != 7):
    print("\nElije una de las siguientes opciones")
    print("1) Inserter nodo")
    print("2) Eliminar nodo")
    print("3) Mostrar nodos")
    print("4) Mostrar dato mayor")
    print("5) Mostrar dato menor")
    print("6) Buscar dato")
    print("7) Salir")
    opcion = int(input("Introduce opcion: "))

    if(opcion == 1):
        dato = int(input("\nIntroduce dato: "))
        abb.agregarNodo(dato)
    elif(opcion == 2):
        dato = int(input("\nIntroduce dato: "))
        abb.eliminarElemento(dato)
    elif(opcion == 3):
        print("\nElige metodo")
        print("1) Preorden")
        print("2) Inorden")
        print("3) Postorden")
        op = int(input("\nIntroduce dato: "))
        if(op == 1):
            print()
            abb.recorridoPreOrden(abb.raiz)
            print()
        elif(op == 2):
            print()
            abb.recorridoOrden(abb.raiz)
            print()
        elif(opcion == 3):
            print()
            abb.recorridoPostOrden(abb.raiz)
            print()
    elif(opcion == 4):
        abb.mostrarDatoMayor()
    elif(opcion == 5):
        abb.mostrarDatoMenor()
    elif(opcion == 6):
        dato = int(input("\nIntroduce dato: "))
        print("\nBuscando . . .")
        if(abb.buscarNodo(dato)):
            print("\nSe encontro el dato")
        else:
            print("\nNo se encontro el dato")
    elif(opcion == 7):
        print("\nSaliendo")
    else:
        print("\nOpcion incorrecta")
        
    
                